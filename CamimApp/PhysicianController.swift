//
//  PhysicianController.swift
//  CamimApp
//
//  Created by Robson Fernandes on 26/12/16.
//  Copyright © 2016 MRF Solution. All rights reserved.
//

import UIKit
import Firebase

class PhysicianController: BaseTableViewController {

    lazy var physicians = PhysicianController.app.databaseRef.child("anchieta").child("physicians")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        physicians.observe(.childAdded, with: { snap in
            if var value = snap.value as? CamimDictionary {
                value["id"] = snap.key
                self.items += [value]
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    override func customCell(cell: UITableViewCell, withObject object: Any?, atIndexPath indexPath: IndexPath) -> UITableViewCell {
        if
            let dictionary = object as? CamimDictionary,
            let name = dictionary["name"] as? String {
            cell.textLabel?.text = name
            cell.detailTextLabel?.text = dictionary["specialty"] as? String
        }
        return cell;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let physician = items[indexPath.row] as? CamimDictionary {
            performSegue(withIdentifier: "PhysicianChat", sender: physician)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ChatController, let physician = sender as? CamimDictionary {
            vc.physician = physician
        }
    }
}
