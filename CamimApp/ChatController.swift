//
//  ChatController.swift
//  CamimApp
//
//  Created by Robson Fernandes on 21/12/16.
//  Copyright © 2016 MRF Solution. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ChatController: BaseTableViewController {

    var physician: CamimDictionary!
    
    @IBOutlet var vwMessageBar: UIView!
    @IBOutlet weak var edtMessage: UITextField!

    private var chatRef: FIRDatabaseReference?
    
    @IBAction func actSend(_ sender: UIButton) {
        guard let message = edtMessage.text, message != "" else {
            return
        }

        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd-HH-mm-ss"

        let chat:CamimDictionary = [
            "sender" : app.senderName,
            "isPatient" : true,
            "message" : message,
            "sentDate" : df.string(from: Date())
        ]
        
        let key = "message_\(items.count)"
        
        chatRef?.child(key).setValue(chat)
        edtMessage.text = ""
    }
    
    var hasObservers = false
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 150
        setupChat()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addObservers()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if animated {
            chatRef?.removeAllObservers()
            vwMessageBar.removeFromSuperview()
        }
    }
    
    func gotoLastRow() {
        if items.count == 0 {
            return
        }
        let indexPath = IndexPath(row: items.count - 1, section: 0)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    var rectBottom = CGRect.zero
    func setupChat() {
        guard var rect = app.window?.bounds else {
            return
        }
        
        title = physician["name"] as? String ?? "Camim"
        
        tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 44, right: 0)
        rect.origin.y = rect.size.height - 44;
        rect.size.height = 44
        navigationController?.view.addSubview(vwMessageBar)
        rectBottom = rect
        vwMessageBar.frame = rect
        
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: .current, using: { notification in
            self.offsetBar(notification: notification)
        })
        
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: .current, using: { notification in
            self.offsetBar(notification: notification)
        })
        
        tableView.isUserInteractionEnabled = true
        tableView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(tapHideKeyboard)))
    }
    
    func tapHideKeyboard() {
        edtMessage.resignFirstResponder()
    }
    
    func offsetBar(notification: Notification) {
        if let info = notification.userInfo,
            let keyboardFrame = info[UIKeyboardFrameEndUserInfoKey] as? CGRect
        {
            
            let isShowingKeyboard = notification.name == .UIKeyboardWillShow
            var rect = rectBottom

            if isShowingKeyboard {
                rect.origin.y -= keyboardFrame.size.height
            }
            
            UIView.animate(withDuration: 0.3, animations: { 
                self.vwMessageBar.frame = rect
            }, completion: { isComplete in
                if isComplete {
                    self.gotoLastRow()
                }
            })
        }
    }
    
    func addObservers() {
        if chatRef != nil {
            return
        }
        
        guard let doctorId = physician["id"] as? String else {
            return
        }
        
        chatRef = app.databaseRef.child("anchieta").child("chats").child(doctorId)
        chatRef?.observe(.childAdded, with: { snap in
            if let dict = snap.value as? CamimDictionary {
                var cm = ChatMessage()
                cm.sender = dict["sender"] as? String ?? ""
                cm.message = dict["message"] as? String ?? ""
                cm.isPatient = cm.sender == self.app.senderName
                self.items += [cm]
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.gotoLastRow()
                }
            }
        })
    }

    override func cellIdentifier(indexPath: IndexPath) -> String {
        if let item = items[indexPath.row] as? ChatMessage, item.isPatient {
            return "cellMe"
        }
        return "cellOther"
    }
    
    override func customCell(cell: UITableViewCell, withObject object: Any?, atIndexPath indexPath: IndexPath) -> UITableViewCell {
        if let cell = cell as? ChatCell, let chat = object as? ChatMessage {
            cell.populate(message: chat)
        }
        return cell;
    }
}

struct ChatMessage {
    var isPatient = true
    var sender = ""
    var message = ""

    var identifier:String {
        return sender == "Robson" ? "chatMe" : "chatOther"
    }
    
    var titleSender: String? {
        return isPatient ? nil : "\(sender) disse:"
    }
    
    var bkgColor: UIColor {
        return isPatient ? .green : .lightGray
    }
}

class ChatCell: UITableViewCell {
    
    @IBOutlet weak var nslMessageWidth: NSLayoutConstraint!
    @IBOutlet weak var nslMessageHeight: NSLayoutConstraint!
    @IBOutlet weak var lblSender: UILabel?
    @IBOutlet weak var vwBubble: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    
    func populate(message: ChatMessage) {
        lblSender?.text = message.titleSender
        vwBubble.backgroundColor = message.bkgColor
        vwBubble.layer.cornerRadius = 8
        nslMessageWidth.constant = UIScreen.main.bounds.size.width - 136
        lblMessage.putText(text: message.message, constraints: nslMessageWidth, nslMessageHeight)
    }
}
