//
//  BaseTableViewController.swift
//  CamimApp
//
//  Created by Robson Fernandes on 26/12/16.
//  Copyright © 2016 MRF Solution. All rights reserved.
//

import UIKit

class BaseTableViewController: BaseStaticTableViewController {
    
    var items = [Any]()

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func cellIdentifier(indexPath: IndexPath) -> String {
        return "cell"
    }
    
    func customCell(cell: UITableViewCell, withObject object: Any? = nil, atIndexPath indexPath: IndexPath) -> UITableViewCell {
        return cell
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier(indexPath: indexPath), for: indexPath)

        return customCell(cell: cell, withObject: items[indexPath.row], atIndexPath: indexPath)
    }
}
