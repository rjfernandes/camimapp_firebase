//
//  MainController.swift
//  CamimApp
//
//  Created by Robson Fernandes on 07/01/17.
//  Copyright © 2017 MRF Solution. All rights reserved.
//

import UIKit

class MainController: BaseStaticTableViewController {
    @IBOutlet weak var edtName: UITextField!

    @IBAction func actEnter(_ sender: UIButton) {
        guard let name = edtName.text , name != "" else {
            return
        }
        
        app.senderName = name
        performSegue(withIdentifier: "IdentificationPhysician", sender: nil)
    }
}
