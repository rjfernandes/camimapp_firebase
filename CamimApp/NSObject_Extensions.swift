//
//  NSObject_Extensions.swift
//  CamimApp
//
//  Created by Robson Fernandes on 07/01/17.
//  Copyright © 2017 MRF Solution. All rights reserved.
//

import UIKit

extension NSObject {
    @nonobjc static var app: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    var app: AppDelegate {
        return NSObject.app
    }
}

extension UILabel {
    var contentHeight: CGFloat {
        return contentSize(offset: CGPoint.init(x: 8, y: 0)).height
    }
    
    func contentSize(maxSize: CGSize? = nil, offset: CGPoint = .zero) -> CGSize {
        guard let text = text, text != "" else {
            return .zero
        }
        
        var size = maxSize ?? bounds.size
        size.height = 1000
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let sizeFrame = NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName : font], context: nil)
        
        return CGSize.init(width: sizeFrame.width + offset.x, height: sizeFrame.height + offset.y)
    }
    
    func putText(text value: Any?, constraints: NSLayoutConstraint...) {
        let cWidth = constraints[0]
        let cHeight = constraints[1]
        if let value = value as? String, value != "" {
            isHidden = false
            text = value
            let maxSize = CGSize.init(width: cWidth.constant, height: 1000)
            let values = contentSize(maxSize: maxSize, offset: CGPoint.init(x: 4, y: 4))
            cWidth.constant = values.width
            cHeight.constant = values.height
        }
        else {
            isHidden = true
            cHeight.constant = 0
        }
    }
}
